const fs = require('fs')
const path = require('path')
const file_path= path.join(path.dirname(process.mainModule.filename), 'data', 'products.json')
module.exports = class Product {
    constructor (title, description, price) {
        this.title = title;
        this.description = description
        this.price = price
    }

    save_product_data () {
        fs.readFile(file_path, (err, fileContent)=>{
            let products = []
            if(!err) {
                products = JSON.parse(fileContent)
            }

            products.push(this)
            fs.writeFile(file_path, JSON.stringify(products), (err)=>{
                console.log(err)
            })
        })


    }
    
    static fetch_all_products (cb) {
        fs.readFile(file_path, (err, fileContent) => {
            if (err){
                cb([])
            }
            else{
                cb(JSON.parse(fileContent))
            }
            
        })
    }
}