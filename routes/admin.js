const express = require ('express')
const path = require ('path')
const router = express.Router()
const adminControllers = require('../controllers/admin')

router.get('/add-product', adminControllers.addProductPage)

router.get('/products', adminControllers.getProducts)

router.post('/add-product', adminControllers.sendProducts)



module.exports = router