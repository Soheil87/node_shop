const express = require ('express')
const path = require ('path')
const bodyParser = require('body-parser')

const admin_route = require('./routes/admin')
const shop_route = require('./routes/shop')

const app = express()

app.set('view engine', 'ejs')
app.set('views', 'views')

app.use(bodyParser.urlencoded({extended:false}))
app.use(express.static(path.join(__dirname, 'public')))

app.use('/admin',admin_route)
app.use(shop_route)


app.use((req, res)=>{
    res.status(404).send('page not found')
})
app.listen(3000)
